import typing as t
from dataclasses import dataclass
import math
from pprint import pprint
from collections import deque


@dataclass
class Data:
    simulation_time: int
    bonus_points: int
    intersections: t.Dict[int, 'Intersection']
    streets: t.Dict[str, 'Street']
    cars: t.Dict[int, 'Car']
    traffic_schedule: 'TrafficSchedule'


@dataclass
class Intersection:
    inflows: t.Set[str]
    outflows: t.Set[str]


@dataclass
class Street:
    begin: int
    end: int
    time_to_cross: int


@dataclass
class TrafficSchedule:
    schedule: t.Dict[int, t.Dict[str, int]]

    @classmethod
    def alternating_one_second(
        cls,
        intersections: t.Dict[int, Intersection],
    ) -> 'TrafficSchedule':
        schedule = {}

        for intersection_id, intersection in intersections.items():
            schedule[intersection_id] = {
                street: 1
                for street in intersection.inflows
            }

        return TrafficSchedule(schedule)

    def is_green(self, data: Data, street: str, time: int):
        intersection_for_street = data.streets[street].end
        total_wait_time = sum(self.schedule[intersection_for_street].values())
        position_in_time = time % total_wait_time

        time_seen = 0
        for curr_street, waiting_time in self.schedule[
                intersection_for_street].items():
            time_seen += waiting_time
            if time_seen > position_in_time:
                if curr_street == street:
                    return True
                return False
        assert False


@dataclass
class Car:
    current_street: str
    time_to_wait: int
    next_streets: t.List[str]
    finished_time: int = -1

    def is_finished(self) -> bool:
        return self.finished_time != -1

    def finish_if_needed(self, time: int):
        if self.time_to_wait == 0 and len(self.next_streets) == 0:
            self.finished_time = time

    def move_to_next_if_needed(self, time: int, data: Data,
                               intersections_full: t.Set[int]):
        if self.is_finished() or self.time_to_wait != 0:
            return

        waiting_intersection = data.streets[self.current_street].end
        if waiting_intersection in intersections_full:
            return

        if not data.traffic_schedule.is_green(data, self.current_street, time):
            return

        next_street = self.next_streets.pop(0)
        next_time = data.streets[next_street].time_to_cross
        self.current_street = next_street
        self.time_to_wait = next_time

        intersections_full.add(waiting_intersection)

    def step_forward_or_finish(self, time: int, data: Data,
                               intersections_full: t.Set[int]):
        if self.is_finished():
            return
        if self.time_to_wait != 0:
            self.time_to_wait -= 1
        self.finish_if_needed(time)
        self.move_to_next_if_needed(time, data, intersections_full)


def read_data(lines: t.Iterator[str]) -> Data:
    intersections = {}
    streets = {}
    cars = {}

    (D, I, S, V, F) = map(int, next(lines).split())

    # reading all the street/intersection data
    for _ in range(S):
        line = next(lines).split()

        B, E = map(int, line[0:2])
        street_name = line[2]
        L = int(line[3])

        streets[street_name] = Street(B, E, L)
        begin_intersection = intersections.setdefault(
            B,
            Intersection(set(), set()),
        )
        begin_intersection.outflows.add(street_name)

        end_intersection = intersections.setdefault(
            E,
            Intersection(set(), set()),
        )
        end_intersection.inflows.add(street_name)

    # reading all the car data
    for i in range(V):
        car_streets = next(lines).split()[1:]
        cars[i] = Car(
            current_street=car_streets[0],
            time_to_wait=0,
            next_streets=car_streets[1:],
        )

    return Data(
        simulation_time=D,
        bonus_points=F,
        intersections=intersections,
        streets=streets,
        cars=cars,
        traffic_schedule=TrafficSchedule.alternating_one_second(intersections),
    )


def run_game(data: Data) -> int:
    for t in range(data.simulation_time + 1):
        intersections_full = set()
        for _, car in data.cars.items():
            car.step_forward_or_finish(t, data, intersections_full)

    total_score = 0
    for _, car in data.cars.items():
        if car.is_finished():
            total_score += data.bonus_points + (data.simulation_time -
                                                car.finished_time)

    return total_score


if __name__ == "__main__":
    total_score = 0

    for example in ['a.txt', 'b.txt', 'c.txt', 'd.txt', 'e.txt', 'f.txt']:
        with open(example) as f:
            lines = f.readlines()
            data = read_data(iter(lines))
            total_score += run_game(data)

    print(total_score)
