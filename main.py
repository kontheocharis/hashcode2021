import math
from pprint import pprint
from collections import OrderedDict

(D, I, S, V, F) = map(int, input().split())



# maps intersection id to the roads that connect on intersection
# keys are intersection ids
# values are outgoing streets
intersection_map = {}
incoming_intersection_map = {}

# keys are streets
# values are (traffic state, intersection id, time it takes to cross street)
traffic_map = {}
outgoing_traffic_map = {}

# reading all the street/intersection data
for _ in range(S):
    line = input().split()

    B, E = map(int, line[0:2])
    street_name = line[2]
    L = int(line[3])

    intersection_map.setdefault(B, []).append(street_name)
    incoming_intersection_map.setdefault(E, []).append(street_name)
    traffic_map[street_name] = (True, E, L)
    outgoing_traffic_map[street_name] = (True, B, L)

# reading all the car data
car_paths = []
for _ in range(V):
    line = input().split()
    car_paths.append(line[1:])

# keys are roads
# values are: dict
#    keys are car indexes
#    values are time to front of intersection --> multiple cars can be at the front
street_cars = {s: OrderedDict({}) for s in traffic_map.keys()}
for i, car_path in enumerate(car_paths):
    first_street = car_path.pop(0)
    street_cars[first_street].update({i: 0})



def transfer_car(car, origin, dest):
    # the car is at the front of the queue in origin

    assert light_state(origin) # make sure light is green
    assert car in street_cars[origin] # car is in the street
    assert next(iter(street_cars[origin].keys()), None) == car # car is first to go
    assert street_cars[origin][car] == 0 # car is already at the end of the road
    assert next_street(car) == dest # make sure the car is supposed to go there

    exits = exits_for_street(origin)
    # print(car)
    # print(exits, origin, dest)
    assert dest in exits # make sure we can go to the street we want

    del street_cars[origin][car] # remove car from origin street
    street_cars[dest].update({car: time_to_cross(dest)}) # add car to dest street
    car_paths[car].pop(0)
    

def flip_light(street_id):
    value = traffic_map[street_id]
    value[0] = not value[0]
    traffic_map[street_id] = value


def light_state(street_id):
    return traffic_map[street_id][0]

def next_street(car):
    return next(iter(car_paths[car]), None)

def time_to_cross(street_id):
    (_, _, time_to_cross) = traffic_map[street_id]
    return time_to_cross

def origin_for_street(street_id):
    (_, intersection_id, *_) = outgoing_traffic_map[street_id]
    return intersection_id
    
def exits_for_street(street_id):
    (_, intersection_id, *_) = traffic_map[street_id]
    return intersection_map[intersection_id]

def first_car_in_street(street_id):
    return next(iter(street_cars[street_id].items()), None)

# returns a dictionary where the keys are intersection ids and the values are
# dictionaries mapping street names to wait time in seconds.
def get_traffic_schedule():
    intersection_weight_mem = {}
    
    def weight_of_intersection(intersection_id, seen=set()):
        if intersection_id in intersection_weight_mem:
            return intersection_weight_mem[intersection_id]
        
        inflows = []
        one_seen = False
        for street in incoming_intersection_map[intersection_id]:
            this_intersection_id = origin_for_street(street)
            if this_intersection_id in seen:
                one_seen = True
                continue
            inflows.append(weight_of_intersection(this_intersection_id, seen=(seen | {intersection_id})) + 1)

        result = sum(map(lambda x: x**(1/1.1), inflows))
        if one_seen:
            result = max(1, result)
        intersection_weight_mem[intersection_id] = result
        return result

    traffic_schedule = {}
    for i in intersection_map.keys():
        incoming_street_weights = list(
            map(weight_of_intersection, map(origin_for_street, incoming_intersection_map[i])))
        sum_of_weights = sum(incoming_street_weights)
        traffic_schedule[i] = OrderedDict({})
        
        lowest_found = math.inf
        for si, street in enumerate(incoming_intersection_map[i]):
            result = 0 if sum_of_weights == 0 else incoming_street_weights[si]/sum_of_weights
            if result > 0 and result < lowest_found:
                lowest_found = result
            traffic_schedule[i][street] = result
        for si, street in enumerate(incoming_intersection_map[i]):
            traffic_schedule[i][street] /= lowest_found
        
    return traffic_schedule
    
def car_can_move(car, elapsed_time, traffic_schedule, origin):
    (_, intersection, *_) = traffic_map[origin]

    # number_of_rules = sum(traffic_schedule[intersection].values())

    wait_time_for_street = elapsed_time % sum(traffic_schedule[intersection].values())

    target_rule = None
    time_seen = 0
    for street, waiting_time in traffic_schedule[intersection].items():
        time_seen += waiting_time
        if time_seen >= wait_time_for_street:
            target_rule = street
    assert target_rule != None
    return target_rule == origin

def main():
    finished_cars = {}
    traffic_schedule = get_traffic_schedule()

    score = 0

    # main simulation loop
    for t in range(0, D):
        #print(f"This is time step {t}:")

        # update all car times
        for (street, cars) in street_cars.items():
            # move first car if need be
            first_entry = first_car_in_street(street)

            if first_entry:
                first_car, time_to_front = first_entry
                if True:
                # if car_can_move(first_car, t, traffic_schedule, street):
                    #print(f"    First car in street {street} is {first_car} with time {time_to_front}")
                    if time_to_front == 0:
                        dest = next_street(first_car)

                        if dest is not None:
                            transfer_car(first_car, street, dest)
                        else:
                            finished_cars.update({first_car: t})
        

        # decrement cars times
        for (street, cars) in street_cars.items():
            for (car, time_to_front) in cars.items():
                if time_to_front > 0:
                    cars[car] -= 1
                    #print(f"    New car time for {car} is {cars[car]}")

    #print(finished_cars)
    for car, time in finished_cars.items():
        score += F + (D - time)
    print(score)


if __name__ == "__main__":
    import sys
    sys.setrecursionlimit(10000)
    main()


# compute the schedules for each interesection and save
# for intersection, streets in results.items():
#     print(f"Weights for intersection {intersection}:")
#     for (street_name, street_value) in streets.items():
#         value = int(street_value)
#         if len(streets) == 1:
#             value = 1
#         print(f"    Weight of street {street_name} is {value}")
#         if len(streets) == 1:
#             break

# for (street, cars) in street_cars.items():
#     for (car, time_to_front) in cars.items():
#         if time_to_front <= 0 and len(car_paths[car]) == 0:
#             score += F + (abs(time_to_front))
#             print(f'Car {car} score is {F} + (abs({time_to_front}))')
#         print(f'Car {car} is in {street} with time {time_to_front} and it still has to traverse: {car_paths[car]}')

